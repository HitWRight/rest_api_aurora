use aurora;
go

create or alter function odoo.GetClients(@id int)
  returns table
as
  return (
  /*
  use aurora;
  go
  Declare @id as int = null
  --*/
    select Account as account_number,
           RegistrationCode as additional_registration_info,
           Address as address,
           BankId as bank,
           ClientGroupId as client_group,
           Comment as comment,
           Email as email,
           Fax as fax,
           Id as id,
           Name as name,
           OrganizationTypeId as organization_type,
           Telephone as phone,
           Code as registration_id,
           VatCode as vat_id,
           VatRegression as vat_regression
      from cli.Clients c
      where (@id is null or c.Id = @id)
  )

create or alter function zabbix.GetScreens()
  returns table
as
  return
  (
    -- select *
    select s.Address as address,
           s.Id as id,
           l.IPv4 as ipv4,
           l.Name as name
      from scr.Screens s
             inner join scr.Logins l on l.ScreenId = s.Id
     where IsDeleted = 0
  )

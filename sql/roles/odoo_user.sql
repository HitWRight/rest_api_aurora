if not exists (select name from master.sys.server_principals where name = 'OdooLogin')
begin
    CREATE LOGIN OdooLogin WITH PASSWORD = N'X9Odoopasw';
end
GO

USE aurora
GO

DROP USER IF EXISTS OdooUser;
GO
DROP ROLE IF EXISTS OdooUserRole;
GO
CREATE ROLE OdooUserRole;
GO

-- GRANT SELECT ON object::cli.Clients TO OdooUserRole;
-- go

grant select on object::odoo.GetClients to OdooUserRole;
go
grant select on object::odoo.GetBanks to OdooUserRole;
go

CREATE USER OdooUser FOR LOGIN OdooUser;
go
ALTER ROLE OdooUserRole ADD MEMBER OdooUser;
go

-- grant select for

--                use aurora;
-- go

  -- exec sp_helprole


  -- drop role OdooUser

pub mod client;
pub mod models;

use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, Deserialize, Serialize)]
pub enum Either<A, B> {
    A(A),
    B(B),
}

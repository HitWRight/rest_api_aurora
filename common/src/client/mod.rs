use crate::models::user::User;
use crate::models::subscriptions::subscriber::Subscriber;
use crate::models::bank::Bank;
use crate::models::company::Company;
use crate::models::screen::Screen;
use log::debug;

// Should be used by Rust API Consumer
#[allow(dead_code)]
pub struct Client {
    base_url: String,
    client: reqwest::Client,
}

// Should be used by Rust API Consumer
#[allow(dead_code)]
impl Client {
    pub fn new(url: &str, user_id: &str, key: &str) -> Self {
        let mut headers = reqwest::header::HeaderMap::new();
        let mut key = reqwest::header::HeaderValue
            ::from_str(&format!("Basic {}",
                                base64::encode(&format!("{}:{}",
                                                        user_id, key))))

            .expect("Failed to set header");

        key.set_sensitive(true);
        println!("{:?}", key);
        headers.insert(reqwest::header::AUTHORIZATION, key);
        let client = reqwest::Client::builder().default_headers(headers)
                                               .build().unwrap();
        Client {
            base_url: url.to_string(),
            client,
        }
    }

    pub async fn get_banks(&self) -> Vec<Bank> {
        let url = format!("{}/banks", self.base_url);
        debug!("Retrieving banks ({})", url);

        let result = self.client.get(&url).send()
                                     .await
                                     .expect(&format!("Failed to GET data ({})", url));
        debug!("{:#?}", result);

        serde_json::from_str(&result.text().await.unwrap()).unwrap()
    }

    pub async fn get_companies(&self) -> Vec<Company> {
        let url = format!("{}/companies", self.base_url);
        debug!("Retrieving companies ({})", url);

        let result = self.client.get(&url).send()
                                          .await
                                          .expect(&format!("Failed to GET data ({})", url));
        debug!("{:#?}", result);

        serde_json::from_str(&result.text().await.unwrap()).unwrap()
    }

    pub async fn get_screen_logins(&self) -> Vec<Screen> {
        let url = format!("{}/screens/logins", self.base_url);
        debug!("Retrieving screen SMB logins ({})", url);

        let result = self.client
                         .get(&url)
                         .send().await
                         .expect(&format!("Failed to GET data ({})", url));
        debug!("{:#?}", result);

        serde_json::from_str(&result.text().await.unwrap()).unwrap()
    }

    pub async fn get_subscriptions_missing_files_subscribers(&self) -> Vec<Subscriber> {
        let url = format!("{}/subscriptions/missing_files/subscribers", self.base_url);
        debug!("Retrieving subscribed users to missing files ({})", url);

        let result = self.client
                         .get(&url)
                         .send().await
                         .expect(&format!("Failed to GET data ({})", url));
        debug!("{:#?}", result);

        serde_json::from_str(&result.text().await.unwrap()).unwrap()
    }

    pub async fn get_subscriptions_missing_files_authorized_users_by_video_number(&self, video_number: &str) -> Vec<User> {
        let url = format!("{}/subscriptions/missing_files/authorized_users_by_video_number/{}", self.base_url, video_number);
        debug!("Retrieving authorized users by missing files video number ({})", url);

        let result = self.client
                         .get(&url)
                         .send().await
                                .expect(&format!("Failed to GET data ({})", url));

        serde_json::from_str(&result.text().await.unwrap()).unwrap()
    }

}

#[cfg(test)]
mod tests {
    use crate::models::user::User;
use crate::models::subscriptions::subscriber::Subscriber;
use crate::models::company::Company;
use crate::models::bank::Bank;
    use crate::models::screen::Screen;
    use crate::models::video_version::VideoVersion;
    use std::net::Ipv4Addr;
    use super::Client;

    #[test]
    fn client_new_successfully_creates_new_client() {
        let _client = Client::new("127.0.0.1:3816", "test", "test");
    }

    #[tokio::test]
    async fn client_get_banks_successfully_retrieve_data() {
        let client = Client::new("http://127.0.0.1:3816", "test", "test");
        let banks = client.get_banks().await;
        assert_eq!(vec![Bank {id: 1, name: "Swedbank".to_string()}], banks);
    }

    #[tokio::test]
    async fn client_get_screen_logins_successfully_retrieve_data() {
        let client = Client::new("http://127.0.0.1:3816", "test", "test");
        let screens = client.get_screen_logins().await;

            assert_eq!(Screen {
                address: "Kaunas".to_string(),
                id: 3,
                ipv4: Ipv4Addr::new(127,0,0,1),
                name: "Ekranas1".to_string(),
                password: "Slaptazodis".to_string(),
                username: "Vartotojas".to_string(),
                video_version: VideoVersion::V2,
            }, screens[0])
    }

    #[tokio::test]
    async fn client_get_companies_succesfully_retrieve_data() {
        let client = Client::new("http://127.0.0.1:3816", "test", "test");
        let companies = client.get_companies().await;

            assert_eq!(Company {
                id: 1,
                name: "Katos Grupe".to_string(),
            }, companies[0])
    }

    #[tokio::test]
    async fn client_get_missing_files_subscribers_successfully_retrieve_data() {
        let client = Client::new("http://127.0.0.1:3816", "test", "test");
        let subscribers = client.get_subscriptions_missing_files_subscribers().await;

            assert_eq!(Subscriber {
                company_id: 1,
                email: "ignas@kata.lt".to_string()
            }, subscribers[0])
    }

    #[tokio::test]
    async fn client_get_missing_files_authorized_users_by_video_number_successfully_retrieve_data() {
        let client = Client::new("http://127.0.0.1:3816", "test", "test");
        let subscribers = client.get_subscriptions_missing_files_authorized_users_by_video_number("8888").await;
            assert_eq!(User {
                company_id: 1,
                email: "ignas@kata.lt".to_string(),
                first_name: "Ignas".to_string(),
                id: 1,
                last_name: "LapÄ?nas".to_string()
            }, subscribers[0])
    }
}

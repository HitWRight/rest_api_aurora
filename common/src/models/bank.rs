use serde::{Deserialize, Serialize};

#[derive(Debug, Deserialize, PartialEq, Serialize)]
pub struct Bank {
    pub id: i32,
    pub name: String,
}

impl Bank {
    pub fn new() -> Bank {
        Bank {
            id: 0,
            name: String::new(),
        }
    }
}

use crate::models::organization_type::OrganizationType;
use crate::models::group::ClientGroup;
use crate::models::bank::Bank;
use crate::Either;
use serde::{Deserialize, Serialize};

#[derive(Debug, Deserialize, Serialize)]
pub struct Client {
    pub account_number: Option<String>,
    pub additional_registration_info: Option<String>,
    pub address: Option<String>,
    pub bank: Option<Either<i32, Bank>>,
    pub client_group: Either<i32, ClientGroup>,
    pub comment: Option<String>,
    pub email: Option<String>,
    pub fax: Option<String>,
    pub id: i32,
    pub name: String,
    pub organization_type: Either<u32, OrganizationType>,
    pub phone: Option<String>,
    pub registration_id: String,
    pub vat_id: Option<String>,
    pub vat_regression: bool,
}

impl Client {
    pub fn new() -> Self {
        Client {
            account_number: None,
            additional_registration_info: None,
            address: None,
            bank: None,
            client_group: Either::A(0),
            comment: None,
            email: None,
            fax: None,
            id: 0,
            name: String::new(),
            organization_type: Either::A(0),
            phone: None,
            registration_id: String::new(),
            vat_id: None,
            vat_regression: false,
        }
    }
}

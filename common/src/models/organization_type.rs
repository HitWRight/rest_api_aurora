use serde::{Deserialize, Serialize};

#[derive(Debug, Deserialize, Serialize)]
pub struct OrganizationType {
    id: u32,
    name: String,
}

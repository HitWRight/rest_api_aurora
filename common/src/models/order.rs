use serde::{Deserialize, Serialize};

#[derive(Debug, Deserialize, Serialize)]
pub struct Order {
    pub id: i32,
    pub number: String,
}

impl Order {
    pub fn new(number: String) -> Order {
        Order {
            id: 0,
            number,
        }
    }
}

use serde::{Deserialize, Serialize};
use std::fmt::Display;
use std::fmt::Formatter;
use std::str::FromStr;

#[derive(Clone, Debug, Deserialize, PartialEq, Serialize)]
pub enum VideoVersion {
    V1,
    V2,
    Tvirtove,
    Urmas,
}

impl FromStr for VideoVersion {
    type Err = ();

    fn from_str(input: &str) -> Result<Self, Self::Err> {
        match input {
            "V1" | "v1" | "1" => Ok(VideoVersion::V1),
            "V2" | "v2" | "2" | "" => Ok(VideoVersion::V2),
            "Tvirtove" | "3" => Ok(VideoVersion::Tvirtove),
            "Urmas" | "URMAS" | "4" => Ok(VideoVersion::Urmas),
            _ => Err(()),
        }
    }
}

impl Display for VideoVersion {
    fn fmt(&self, f: &mut Formatter) -> std::fmt::Result {
        write!(
            f,
            "{}",
            match self {
                VideoVersion::V1 => "V1",
                VideoVersion::V2 => "V2",
                VideoVersion::Tvirtove => "Tvirtove",
                VideoVersion::Urmas => "Urmas",
            }
        )
    }
}

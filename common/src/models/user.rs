use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, Deserialize, PartialEq, Serialize)]
pub struct User {
    pub company_id: i32,
    pub email: String,
    pub first_name: String,
    pub id: i32,
    pub last_name: String,
}

impl User {
    pub fn new(first_name: String, last_name: String, email: String) -> Self {
        User {
            company_id: 0,
            email,
            first_name,
            id: 0,
            last_name,
        }
    }
}

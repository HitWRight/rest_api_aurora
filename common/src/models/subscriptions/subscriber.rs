use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, Deserialize, PartialEq, Serialize)]
pub struct Subscriber {
    pub company_id: i32,
    pub email: String,
}

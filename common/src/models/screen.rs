use crate::models::video_version::VideoVersion;
use serde::{Deserialize, Serialize};
use std::net::Ipv4Addr;

#[derive(Clone, Debug, Deserialize, PartialEq, Serialize)]
pub struct Screen {
    pub address: String,
    pub id: i32,
    pub ipv4: Ipv4Addr,
    pub name: String,
    pub password: String,
    pub username: String,
    pub video_version: VideoVersion,
}

impl Screen {
    pub fn new(name: String) -> Screen {
        Screen {
            address: "".to_string(),
            id: 0,
            ipv4: Ipv4Addr::new(127, 0, 0, 1),
            name,
            password: "".to_string(),
            username: "".to_string(),
            video_version: VideoVersion::V2,
        }
    }
}

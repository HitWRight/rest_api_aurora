use odbc::*;

pub struct Database<'env> {
    pub connection: Connection<'env>,
}

impl Database<'_> {
    // pub fn init() -> Result std::result::Result<Environment<Version3>, Option<DiagnosticRecord>>
    pub fn new<'env>(
        environment: &'env Environment<Version3>,
        config: &crate::Configuration,
    ) -> Result<Database<'env>> {
        let database = Database {
            // environment,
            connection: environment.connect_with_connection_string(&format!(
                "{}{}{}{}{}",
                "Driver=ODBC Driver 17 for SQL Server;",
                format!("Server=tcp:{},1433;", config.server),
                format!("Database={};", config.database),
                format!("UID={};", config.username),
                format!("PWD={};", config.password),
            ))?,
        };

        Ok(database)
    }
}

pub trait Model {
    fn parse_cursor_row(
        cursor: &mut odbc::Cursor<'_, '_, '_, odbc::Allocated>,
    ) -> Self
    where
        Self: std::marker::Sized;

    fn get_all(database: Database) -> Vec<Self>
    where
        Self: std::marker::Sized;

    fn get(database: Database, key: i32) -> Self
    where
        Self: std::marker::Sized;
}

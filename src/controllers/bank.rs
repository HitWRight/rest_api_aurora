use crate::database::Database;
use crate::Configuration;
use crate::Model;
use actix_web::http::header::ContentType;
use actix_web::{web, HttpResponse, Responder};
use aurora_api_common::models::bank::Bank;
use odbc::create_environment_v3;
use log::debug;

pub async fn get_all(config: web::Data<Configuration>) -> impl Responder {
    let environment = create_environment_v3()
        .map_err(|e| e.unwrap())
        .expect("Failed to provide ODBC environment");
    let database =
        Database::new(&environment, config.get_ref()).expect("Failed to connect to the database");

    let result: Vec<Bank> = Model::get_all(database);
    debug!("Retrieve banks: {:#?}", result);
    HttpResponse::Ok()
        .set(ContentType::json())
        .body(serde_json::to_string_pretty(&result).expect("Failed to serialize banks list"))
}

use crate::database::Database;
use crate::Configuration;
use crate::Model;
use actix_web::http::header::ContentType;
use actix_web::{web, HttpResponse, Responder};
use aurora_api_common::models::subscriptions::subscriber::Subscriber;
use aurora_api_common::models::user::User;
use odbc::create_environment_v3;

pub async fn missing_files_get_emails(config: web::Data<Configuration>) -> impl Responder {
    let environment = create_environment_v3()
        .map_err(|e| e.unwrap())
        .expect("Failed to provide ODBC environment");

    let database =
        Database::new(&environment, config.get_ref()).expect("Failed to connect to the database");

    let result: Vec<Subscriber> = Model::get_all(database);
    HttpResponse::Ok()
        .set(ContentType::json())
        .body(serde_json::to_string(&result).expect("Failed to serialize screen list"))
}

#[derive(serde::Deserialize)]
pub struct ByVideoNumber {
    video_number: String,
}

pub async fn missing_files_get_users_by_video_number(
    info: web::Path<ByVideoNumber>,
    config: web::Data<Configuration>,
) -> impl Responder {
    let environment = create_environment_v3()
        .map_err(|e| e.unwrap())
        .expect("Failed to provide ODBC environment");

    let database =
        Database::new(&environment, config.get_ref()).expect("Failed to connect to the database");

    let result: Vec<User> = crate::models::user::get_by_video_number(database, &info.video_number);
    HttpResponse::Ok()
        .set(ContentType::json())
        .body(serde_json::to_string(&result).expect("Failed to serialize screen list"))
}

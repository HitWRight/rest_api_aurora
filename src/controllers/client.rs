use crate::database::Database;
use crate::Configuration;
use crate::Model;
use actix_web::http::header::ContentType;
use actix_web::{web, HttpResponse, Responder};
use aurora_api_common::models::client::Client;
use odbc::create_environment_v3;

pub async fn get_all(config: web::Data<Configuration>) -> impl Responder {
    let environment = create_environment_v3()
        .map_err(|e| e.unwrap())
        .expect("Failed to provide ODBC environment");
    let database =
        Database::new(&environment, config.get_ref()).expect("Failed to connect to the database");

    let result: Vec<Client> = Model::get_all(database);
    HttpResponse::Ok()
        .set(ContentType::json())
        .body(serde_json::to_string_pretty(&result).expect("Failed to serialize clients list"))
}

#[derive(serde_derive::Deserialize)]
pub struct GetClient {
    id: i32,
}

pub async fn get(info: web::Path<GetClient>, config: web::Data<Configuration>) -> impl Responder {
    let environment = create_environment_v3()
        .map_err(|e| e.unwrap())
        .expect("Failed to provide ODBC environment");
    let database =
        Database::new(&environment, config.get_ref()).expect("Failed to connect to the database");

    let client: Client = Model::get(database, info.id);

    HttpResponse::Ok()
        .set(ContentType::json())
        .body(serde_json::to_string_pretty(&client).expect("Failed to serialize client"))
}

use crate::database::Database;
use crate::Configuration;
use crate::Model;
use actix_web::http::header::ContentType;
use actix_web::{web, HttpResponse, Responder};
use aurora_api_common::models::company::Company;
use aurora_api_common::models::order::Order;
use odbc::create_environment_v3;

pub async fn get_all(config: web::Data<Configuration>) -> impl Responder {
    let environment = create_environment_v3()
        .map_err(|e| e.unwrap())
        .expect("Failed to provide ODBC environment");

    let database =
        Database::new(&environment, config.get_ref()).expect("Failed to connect to the database");

    let result: Vec<Company> = Model::get_all(database);
    HttpResponse::Ok()
        .set(ContentType::json())
        .body(serde_json::to_string(&result).expect("Failed to serialize screen list"))
}

#[derive(serde_derive::Deserialize)]
pub struct GetCompany {
    id: i32,
}

pub async fn get_orders(
    info: web::Path<GetCompany>,
    config: web::Data<Configuration>,
) -> impl Responder {
    let environment = create_environment_v3()
        .map_err(|e| e.unwrap())
        .expect("Failed to provide ODBC environment");

    let database =
        Database::new(&environment, config.get_ref()).expect("Failed to connect to the database");

    let result: Vec<Order> = crate::models::order::get_by_company(database, info.id);
    HttpResponse::Ok()
        .set(ContentType::json())
        .body(serde_json::to_string(&result).expect("Failed to serialize screen list"))
}

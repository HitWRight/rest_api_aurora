use odbc::Data;
use odbc::Statement;
use crate::database::{Database, Model};
use aurora_api_common::models::order::Order;

impl Model for Order {
    fn parse_cursor_row(cursor: &mut odbc::Cursor<'_, '_, '_, odbc::Allocated>) -> Self {
        Order {
            id: cursor
                .get_data(1)
                .expect("Failed to read id")
                .expect("Expecting a non null value"),
            number: cursor
                .get_data(2)
                .expect("Failed to read number")
                .expect("Expecting a non null value"),
        }
    }
    fn get_all(database: Database<'_>) -> std::vec::Vec<Self> {
        let mut result = Vec::<Self>::new();
        let statement = Statement::with_parent(&database.connection)
            .expect("Failed to use provided database connection");

        match statement
            .exec_direct("select * from rest.scr_GetOrders()")
            .expect("Failed to execute sql query")
        {
            Data(mut statement) => {
                while let Some(mut cursor) = statement.fetch().expect("Connection failed mid-read")
                {
                    result.push(Self::parse_cursor_row(&mut cursor))
                }
            }
            _no_data => panic!("No data returned by function"),
        }

        result
    }
    fn get(_: Database<'_>, _: i32) -> Self {
        todo!()
    }
}

pub fn get_by_company(database: Database<'_>, company_id: i32) -> std::vec::Vec<Order> {
    let mut result = Vec::<Order>::new();
    let statement = Statement::with_parent(&database.connection)
        .expect("Failed to use provided database connection");
    let statement = statement
        .bind_parameter(1, &company_id)
        .expect("Failed to bind parameter");
    match statement
        .exec_direct("select * from rest.aur_getOrdersByCompany(?)")
        .expect("Failed to execute sql query")
    {
        Data(mut statement) => {
            while let Some(mut cursor) = statement.fetch().expect("connection failed mid-read") {
                result.push(Order::parse_cursor_row(&mut cursor))
            }
        }
        _no_data => panic!("No data returned"),
    }
    result
}

use odbc::Data;
use odbc::Statement;
use crate::database::{Database, Model};
use aurora_api_common::models::bank::Bank;

impl Model for Bank {

    fn parse_cursor_row(
        cursor: &mut odbc::Cursor<'_, '_, '_, odbc::Allocated>,
    ) -> Self {
        Bank {
            id: cursor
                .get_data::<i32>(1)
                .expect("Failed to read id")
                .expect("Expecting a non null value for id"),
            name: cursor
                .get_data::<String>(2)
                .expect("Failed to read name")
                .expect("Expecting a non null name"),
        }
    }

    fn get_all(database: Database<'_>) -> std::vec::Vec<Self> {
        let mut result = Vec::<Self>::new();
        let statement = Statement::with_parent(&database.connection)
            .expect("Failed to use provided database connection");

        match statement
            .exec_direct("select * from cli.Banks")
            .expect("Failed to execute sql query")
        {
            Data(mut statement) => {
                while let Some(mut cursor) = statement.fetch().expect("Connection failed mid-read")
                {
                    result.push(Self::parse_cursor_row(&mut cursor))
                }
            }
            _no_data => panic!("No data returned by function"),
        }

        result
    }

    fn get(_: Database<'_>, _: i32) -> Self {
        todo!()
    }
}

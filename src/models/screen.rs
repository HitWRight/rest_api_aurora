use crate::database::{Database, Model};
use aurora_api_common::models::screen::Screen;
use aurora_api_common::models::video_version::VideoVersion;
use odbc::{Data, Statement};
use std::net::Ipv4Addr;
use std::str::FromStr;

impl Model for Screen {
    fn get_all(database: Database<'_>) -> std::vec::Vec<Self> {
        let mut result = Vec::<Self>::new();
        let statement = Statement::with_parent(&database.connection)
            .expect("Failed to use provided database connection");

        match statement
            .exec_direct("select * from rest.scr_GetLogins()")
            .expect("Failed to execute sql query")
        {
            Data(mut statement) => {
                while let Some(mut cursor) = statement.fetch().expect("Connection failed mid-read")
                {
                    result.push(Self::parse_cursor_row(&mut cursor))
                }
            }
            _no_data => panic!("No data returned by function"),
        }

        result
    }

    fn get(_database: Database<'_>, _: i32) -> Self {
        todo!()
    }

    fn parse_cursor_row(
            cursor: &mut odbc::Cursor<'_, '_, '_, odbc::Allocated>,
    ) -> Self {
        Screen {
            address: cursor
                .get_data::<String>(1)
                .expect("Failed to read Address")
                .expect("Expecting a non null value for address"),
            id: cursor
                .get_data::<i32>(2)
                .expect("Failed to read id")
                .expect("Expecting a non null value"),
            ipv4: {
                let result = cursor
                    .get_data::<Vec<u8>>(3)
                    .expect("Failed to read ip address")
                    .expect("Expecting a non null value for ip");
                Ipv4Addr::new(result[0], result[1], result[2], result[3])
            },
            name: cursor
                .get_data::<String>(4)
                .expect("Failed to read name")
                .expect("Expecting a non null value"),
            password: cursor
                .get_data::<String>(5)
                .expect("Failed to read password")
                .expect("Expecting a non null value"),
            username: cursor
                .get_data::<String>(6)
                .expect("Failed to read password")
                .expect("Expecting a non null value"),
            video_version: VideoVersion::from_str(
                &cursor
                    .get_data::<String>(7)
                    .expect("Failed to read video_version")
                    .expect("Expecting a non null version"),
            )
            .expect("Failed to parse video_versiont to the provided enumerable"),
        }
    }
}

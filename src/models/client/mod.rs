use crate::database::Database;
use crate::database::Model;
use aurora_api_common::models::client::Client;
use aurora_api_common::Either;
use odbc::Data;
use odbc::Statement;

impl Model for Client {
    fn get_all(database: Database<'_>) -> std::vec::Vec<Self> {
        let mut result = Vec::<Self>::new();
        let statement = Statement::with_parent(&database.connection)
            .expect("Failed to use provided database connection");

        match statement
            .exec_direct("select * from aurora.odoo.GetClients(null)")
            .expect("Failed to execute sql query")
        {
            Data(mut statement) => {
                while let Some(mut cursor) = statement.fetch().expect("Connection failed mid-read")
                {
                    result.push(Self::parse_cursor_row(&mut cursor))
                }
            }
            _no_data => panic!("No data returned by function"),
        }

        result
    }

    fn get(database: Database<'_>, id: i32) -> Self {
        let statement = Statement::with_parent(&database.connection)
            .expect("Failed to use provided database connection");
        let statement = statement
            .bind_parameter(1, &id)
            .expect("Failed to bind parameter");
        match statement
            .exec_direct("select * from aurora.odoo.GetClients(?)")
            .expect("Failed to execute sql query")
        {
            Data(mut statement) => {
                if let Some(mut cursor) = statement.fetch().expect("Connection failed mid-read") {
                    return Self::parse_cursor_row(&mut cursor);
                }
            }
            _no_data => panic!("No data returned by function"),
        }
        Client::new()
    }

    fn parse_cursor_row(
        cursor: &mut odbc::Cursor<'_, '_, '_, odbc::Allocated>,
    ) -> Client {
        let mut client = Client::new();
        client.account_number = cursor
            .get_data::<String>(1)
            .expect("Failed to account number");
        client.additional_registration_info = cursor
            .get_data::<String>(2)
            .expect("Failed to read additional registration info");
        client.address = cursor
            .get_data::<String>(3)
            .expect("Failed to read address");
        client.bank = match cursor.get_data::<i32>(4).expect("Failed to read bank") {
            Some(data) => Some(Either::A(data)),
            None => None,
        };
        client.client_group = Either::A(
            cursor
                .get_data::<i32>(5)
                .expect("Failed to read client group")
                .expect("Expecting non null value"),
        );
        client.comment = cursor
            .get_data::<String>(6)
            .expect("failed to read comment");
        client.email = cursor.get_data::<String>(7).expect("Failed to read email");
        client.fax = cursor.get_data::<String>(8).expect("Failed to read fax");
        client.id = cursor
            .get_data::<i32>(9)
            .expect("Failed to read id")
            .expect("Expecting a non null value");
        client.name = cursor
            .get_data::<String>(10)
            .expect("Failed to read name")
            .expect("Expecting non null value");
        client.organization_type = Either::A(
            cursor
                .get_data(11)
                .expect("Failed to read organization type")
                .expect("Expecting a non null value"),
        );
        client.phone = cursor
            .get_data::<String>(12)
            .expect("Failed to read phone data");
        client.registration_id = cursor
            .get_data::<String>(13)
            .expect("Failed to read client registration id")
            .expect("Expecting a non null value");
        client.vat_id = cursor
            .get_data::<String>(14)
            .expect("Failed to read vat id");
        client.vat_regression = cursor
            .get_data::<bool>(15)
            .expect("Failed to read var regression")
            .expect("Can't be null");
        // client.vat_regression = cursor.get_data(13).unwrap().unwrap();
        client
    }
}

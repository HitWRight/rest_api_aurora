use crate::database::{Database, Model};
use aurora_api_common::models::subscriptions::subscriber::Subscriber;
use odbc::Data;
use odbc::Statement;

impl Model for Subscriber {
    fn parse_cursor_row(cursor: &mut odbc::Cursor<'_, '_, '_, odbc::Allocated>)
                        -> Self {
        Subscriber {
            company_id: cursor
                .get_data::<i32>(1)
                .expect("Failed to read company_id")
                .expect("Expecting a non null value for company_id"),
            email: cursor
                .get_data::<String>(2)
                .expect("Failed to read first email")
                .expect("Expecting a non null value for email")
        }
    }

    fn get_all(database: Database<'_>) -> std::vec::Vec<Self> {
        let mut result = Vec::<Self>::new();
        let statement = Statement::with_parent(&database.connection)
            .expect("Failed to use provided database connection");

        match statement
            .exec_direct("select * from rest.aur_Subscription_MissingFiles_GetSubscribedEmails()")
            .expect("Failed to execute sql query")
        {
            Data(mut statement) => {
                while let Some(mut cursor) = statement.fetch().expect("connection failed mid-read")
                {
                    result.push(Self::parse_cursor_row(&mut cursor));
                }
            }
            _no_data => panic!("No data returned"),
        }
        result
    }

    fn get(_database: Database<'_>, _: i32) -> Self {
        todo!()
    }
}

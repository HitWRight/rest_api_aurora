use crate::database::{Database, Model};
use aurora_api_common::models::user::User;
use odbc::Data;
use odbc::Statement;

impl Model for User {
    fn parse_cursor_row(cursor: &mut odbc::Cursor<'_, '_, '_, odbc::Allocated>) -> Self {
        User {
            company_id: cursor
                .get_data::<i32>(1)
                .expect("Failed to red company_id")
                .expect("Expecting a non nll value for company_id"),
            email: cursor
                .get_data::<String>(2)
                .expect("Failed to read email")
                .expect("Expecting a non null value for email"),
            first_name: cursor
                .get_data::<String>(3)
                .expect("Failed to read first name")
                .expect("Expecting a non null value for first_name"),
            id: cursor
                .get_data::<i32>(4)
                .expect("Failed to read id")
                .expect("Expecting a non null value for id"),
            last_name: cursor
                .get_data::<String>(5)
                .expect("Failed to read last name")
                .expect("Expecting a non null value for last_name"),
        }
    }

    fn get_all(database: Database<'_>) -> std::vec::Vec<Self> {
        let mut result = Vec::<Self>::new();
        let statement = Statement::with_parent(&database.connection)
            .expect("Failed to use provided database connection");

        match statement
            .exec_direct("select * from rest.com_GetUsers()")
            .expect("Failed to execute sql query")
        {
            Data(mut statement) => {
                while let Some(mut cursor) = statement.fetch().expect("connection failed mid-read")
                {
                    result.push(Self::parse_cursor_row(&mut cursor));
                }
            }
            _no_data => panic!("No data returned"),
        }
        result
    }
    fn get(_database: Database<'_>, _: i32) -> Self {
        todo!()
    }
}

pub fn get_by_video_number(database: Database<'_>, video_number: &str) -> std::vec::Vec<User> {
    let mut result = Vec::<User>::new();
    let statement = Statement::with_parent(&database.connection)
        .expect("Failed to use provided database connection");
    let statement = statement.bind_parameter(1, &video_number).unwrap();
    match statement
        .exec_direct("select * from rest.aur_GetAuthorizedPeopleByVideoId(?)")
        .expect("Failed to execute sql query")
    {
        Data(mut statement) => {
            while let Some(mut cursor) = statement.fetch().expect("connection failed mid-read") {
                result.push(User::parse_cursor_row(&mut cursor))
            }
        }
        _no_data => panic!("No data returned"),
    }
    result
}

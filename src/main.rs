extern crate env_logger;
extern crate odbc;

mod controllers;
mod database;
mod models;

use actix_web::Error;
use actix_web::dev::ServiceRequest;
use actix_web::middleware::Logger;
use actix_web::{web, App, HttpServer};
use actix_web_httpauth::extractors::AuthenticationError;
use actix_web_httpauth::extractors::basic::BasicAuth;
use actix_web_httpauth::extractors::basic::Config;
use actix_web_httpauth::middleware::HttpAuthentication;
use clap::Clap;
use crate::controllers::*;
use database::Model;
use std::borrow::Cow;

#[derive(Clap, Clone, Debug)]
#[clap(author = "Ignas Lapėnas <ignas@lapenas.dev>")]
pub struct Configuration {
    /// Ipv4 address for connecting to the MsSql server
    #[clap(short = 's', long = "server", default_value = "127.0.0.1")]
    server: String,
    /// MsSql database name
    #[clap(short = 'd', long = "database", default_value = "auroradb")]
    database: String,
    /// MsSql server user name
    #[clap(short = 'u', long = "username", default_value = "sa")]
    username: String,
    /// MsSql server user pass
    #[clap(short = 'p', long = "password", default_value = "X9Baibokas")]
    password: String,
    /// Bind address
    #[clap(short = 'b', long = "bind_address", default_value = "127.0.0.1:3816")]
    bind: String,
}

#[actix_rt::main]
async fn main() -> std::io::Result<()> {
    env_logger::init();

    let config = Configuration::parse();
    let bind = config.bind.clone();
    let logins = read_credentials().await;

    HttpServer::new(move || {
        App::new()
            .wrap(Logger::default())
            .wrap(HttpAuthentication::basic(basic_auth_validator))
            .data(config.clone())
            .app_data(logins.clone())
            .route("/banks", web::get().to(bank::get_all))
            .route("/clients", web::get().to(client::get_all))
            .route("/clients/{id}", web::get().to(client::get))
            .route("/companies", web::get().to(company::get_all))
            .route("/companies/{id}/orders", web::get().to(company::get_orders))
            .route("/screens/logins", web::get().to(screen::get_logins))
            .route(
                "/subscriptions/missing_files/subscribers",
                web::get().to(subscription::missing_files_get_emails),
            )
            .route(
                "/subscriptions/missing_files/authorized_users_by_video_number/{video_number}",
                web::get().to(subscription::missing_files_get_users_by_video_number),
            )
    })
    .bind(bind)?
    .run()
    .await
}

#[derive(Clone, Debug)]
struct Logins {
    credentials: Vec<Credential>
}

#[derive(Clone, Debug)]
struct Credential {
    user: String,
    key: String
}

async fn read_credentials() -> Logins {
    let contents = std::fs::read_to_string("credentials").expect("Something went wrong reading file");
    Logins {
        credentials: contents.split("\n")
                             .filter(|line| line.trim() != "")
                             .map(|line| {
                                 let parts : Vec<&str>= line.split(":").collect();
                                 println!("{:?}", parts);
                                 Credential {
                                     user: parts[0].to_string(),
                                     key: parts[1].to_string()
                                 }
                             }).collect()
    }
}

async fn basic_auth_validator(req: ServiceRequest, credentials: BasicAuth)
                              -> Result<ServiceRequest, Error> {
    println!("{:?}", credentials);
    println!("{:?}", credentials.password());
    let config = req
        .app_data::<Config>()
        .map(|data| data.clone())
        .unwrap_or_else(Default::default);


    println!("{:?}", config);
    let logins = req.app_data::<Logins>().map(|data| data.clone()).unwrap();
    println!("logins: {:?}", logins);
    if logins.credentials.iter()
                         .any(|cred|
                              &Cow::from(cred.user.clone()) == credentials.user_id() &&
                              &Cow::from(cred.key.clone()) == credentials.password()
                                                                         .unwrap_or(&Cow::from("")))
    {
        println!("Logged in as: {}", credentials.user_id());
        return Ok(req)
    }

    Err(AuthenticationError::from(config).into())
}

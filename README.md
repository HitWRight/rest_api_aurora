

# Aprašymas

Rust pasiremta technologija.  
Tai elementarus wrapper'is pasinaudojant MSSQL Server ODBC driver'iu.  
Pagrindinė paskirtis atkelti serverį į web'ui lengvai pasiekiamą erdvę.  
Sumažinti attack surface kodui ir duomenų prieigai, bei sukurti galimybę integruoti serverį  
su kita programinė įranga.  

Ilgalaikis planas Peršokti nuo MSSQL server'io prie psql serverio.  


# Reikalavimai

-   MSSQL 2017 Server ODBC driver.
-   aurora\_api\_common biblioteka.


# Kompiliavimas

-   Susidiegiami Rust compiliavimo įrankiai (Rustup)
-   paleidžiama "cargo build komanda"


# Instaliavimas

-   Nukopijuojamas binary failas (.exe jei po windows)
-   Sekamos programos instrukcijos konfiguruojant connection


# Testavimas

Nėra (Numatyta naudot cargo test)  

